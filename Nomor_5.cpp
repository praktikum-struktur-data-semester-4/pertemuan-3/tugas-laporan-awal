/*
	Dibuat oleh:
	Nama: Zulfahmi Trimahardika
	NIM: 201011400434
	Kelas: 04TPLE007
*/

#include <iostream>
using namespace std;
const int nilai = 3;
 
int main () {
    int  a[nilai] = {1, 2, 3};
    int *ptr[nilai];

    for (int i = 0; i < nilai; i++) {
        ptr[i] = &a[i];
    }

    for (int i = 0; i < nilai; i++) {
        cout << "Nilai array a[" << i << "] = ";
        cout << *ptr[i] << endl;
    }
   
    return 0;
}
